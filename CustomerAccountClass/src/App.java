public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Nam", 10);
        Customer customer2 = new Customer(2, "Lan", 15);
        System.out.println("Customer 1 la: " + customer1);
        System.out.println("Customer 2 la: " + customer2);
        System.out.println(" ");
        Account account1 = new Account(11, customer1, 10000);
        Account account2 = new Account(22, customer2, 20000);
        System.out.println("Account 1 la: " + account1);
        System.out.println("Account 2 la: " + account2);
    }
}
